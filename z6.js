db.people.insertOne({
    "sex" : "Male",
    "first_name" : "Adrian",
    "last_name" : "Szopiak",
    "job" : "Android App Dev",
    "email" : "s21739@pjwstk.edu.pl",
    "location" : {
        "city" : "WAW",
        "address" : {
            "streetname" : "Wiejska",
            "streetnumber" : "1"
        }
    },
    "description" : "huehue",
    "height" : 180,
    "weight" : 90,
    "birth_date" : "1992-01-01T00:00:00Z",
    "nationality" : "Poland",
    "credit" : [
        {
			"type" : "jcb",
			"number" : "4017957170327",
			"currency" : "RUB",
			"balance" : "4463.86"
		}
    ]
})

printjson(db.people.findOne(
	{
		last_name : {$eq: "Szopiak"}	
	}
))